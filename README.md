# Interpretador automatizado de amostragem de solo

<br>
<b>Objetivo do produto:</b><br>
	Hoje há poucas bibliotecas específicas ao agronegócio, para auxiliar a tomada de decisão, então nosso objetivo é desenvolver uma aplicação em python para criar indicadores dos valores baixo, médio ou alto relacionado a fertilidade do solo. <br>
<br>
<b>Instalando Python</b><br>

Tutorial do download e instalação do python:<br>
`Windows:` https://python.org.br/instalacao-windows/<br>
`Linux:` https://python.org.br/instalacao-linux/<br>
<br>

<b>Clonando o projeto</b><br>
```
git clone git@gitlab.com:BDAg/pysolo.git
cd pysolo
```

<b>Instalando as bibliotecas</b><br>
```
pip3 install matplotlib pandas numpy
```
